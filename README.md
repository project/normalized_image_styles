## Introduction

The Normalized Image Styles module provides several sets of aspect ratio based image styles with normalized dimensions.

The primary use case for this module is:

- Responsive Image style sets

## Requirements

- Image Style Generate - https://www.drupal.org/project/image_style_generate
- Image Style Quality - https://www.drupal.org/project/image_style_quality
- Focal Point - https://www.drupal.org/project/focal_point
- Migrate Plus - https://www.drupal.org/project/migrate_plus
- Migrate Tools - https://www.drupal.org/project/migrate_tools

## Installation

Install the Normalized Image Styles module as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

Enable the module at /admin/modules#edit-modules-normalized-image-styles.

## Configuration

- Enable the Normalized Image Styles module and any desired aspect ratios (listed in descending order from widest to narrowest aspect
  ratio) at /admin/modules
- Execute the migration imports for the desired aspect ratio sets at
  /admin/structure/migrate/manage/normalized_image_styles/migrations -OR-
- Execute the migration imports for the desired aspect ratio sets via Drush commands (_drush migrate:status_ to list machine names,
  or _drush migrate:import --tag normalized_ to import all enabled styles)
- Once imported, Normalized Image Styles and all of its sub-modules can safely be uninstalled at /admin/modules/uninstall
- The generated image style configuration will persist and can be managed as normal at /admin/config/development/configuration
- You can also remove Normalized Image Styles from composer.json (_composer remove drupal/normalized_image_styles_), but be sure to keep both Focal Point and Image Style Quality (_composer require drupal/focal_point drupal/image_style_quality_) as these will continue to be required by the generated image styles.

## Maintainers

Current maintainers for Drupal 10:

- Perry Brown (UpTil4Music) - https://www.drupal.org/u/UpTil4Music
